package com.microservice.shipping.repository;

import com.microservice.shipping.entity.ShippingTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShippingTransactionRepository extends JpaRepository<ShippingTransaction,Integer> {
}
