package com.microservice.shipping.service;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.microservice.shipping.entity.ProductPostalCode;
import com.microservice.shipping.entity.ShippingTransaction;
import com.microservice.shipping.repository.ProductPostalCodeRepository;
import com.microservice.shipping.repository.ShippingTransactionRepository;
import com.util.commons.dto.OrderRequestDto;
import com.util.commons.dto.ShippingRequestDto;
import com.util.commons.event.OrderEvent;
import com.util.commons.event.ShippingEvent;
import com.util.commons.status.ShippingStatus;

@Service
public class ShippingService {

    @Autowired
    private ShippingTransactionRepository shippingTransactionRepository;

    @Autowired
    private ProductPostalCodeRepository productPostalCodeRepository;


    @PostConstruct
    public void initUserBalanceInDB() {
        productPostalCodeRepository.saveAll(Stream.of(new ProductPostalCode(200, 11000),
                									  new ProductPostalCode(201, 11001),
                									  new ProductPostalCode(202, 11002),
                									  new ProductPostalCode(203, 11003),
                									  new ProductPostalCode(204, 11004),
                									  new ProductPostalCode(205, 11005)).collect(Collectors.toList()));
    }

    @Transactional
    public ShippingEvent newOrderEvent(OrderEvent orderEvent) {
        OrderRequestDto orderRequestDto = orderEvent.getOrderRequestDto();

        ShippingRequestDto shippingRequestDto = new ShippingRequestDto(orderRequestDto.getOrderId(),orderRequestDto.getUserId(), orderRequestDto.getPostalCode());

        return productPostalCodeRepository.findById(orderRequestDto.getProductId())
                .filter(ub ->
                        ub.getProductId().intValue() == orderRequestDto.getProductId().intValue() &&
                        ub.getPostalCodeId().intValue() == orderRequestDto.getPostalCode().intValue())
                .map(ub -> {
                	shippingTransactionRepository.save(new ShippingTransaction(orderRequestDto.getOrderId(), orderRequestDto.getProductId(), orderRequestDto.getPostalCode()));
                    return new ShippingEvent(shippingRequestDto, ShippingStatus.SHIPPING_COMPLETED);
                }).orElse(new ShippingEvent(shippingRequestDto, ShippingStatus.SHIPPING_FAILED));

    }

    @Transactional
    public void cancelOrderEvent(OrderEvent orderEvent) {

        shippingTransactionRepository.findById(orderEvent.getOrderRequestDto().getOrderId())
                .ifPresent(ut->{
                    shippingTransactionRepository.delete(ut);
                });
    }
}
