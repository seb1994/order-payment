package com.microservice.shipping.config;

import com.microservice.shipping.service.ShippingService;
import com.util.commons.event.OrderEvent;
import com.util.commons.event.ShippingEvent;
import com.util.commons.status.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@Configuration
public class ShippingConsumerConfig {

    @Autowired
    private ShippingService shippingService;

    @Bean
    public Function<Flux<OrderEvent>, Flux<ShippingEvent>> shippingProcessor() {
        return orderEventFlux -> orderEventFlux.flatMap(this::processShipping);
    }

    private Mono<ShippingEvent> processShipping(OrderEvent orderEvent) {

        if(OrderStatus.ORDER_CREATED.equals(orderEvent.getOrderStatus())){
            return  Mono.fromSupplier(()->this.shippingService.newOrderEvent(orderEvent));
        }else{
            return Mono.fromRunnable(()->this.shippingService.cancelOrderEvent(orderEvent));
        }
    }
}
