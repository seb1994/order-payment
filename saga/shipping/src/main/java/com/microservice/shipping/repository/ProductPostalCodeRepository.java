package com.microservice.shipping.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservice.shipping.entity.ProductPostalCode;

public interface ProductPostalCodeRepository  extends JpaRepository<ProductPostalCode,Integer> {
}
