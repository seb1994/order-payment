package com.microservice.shipping.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data

public class ProductPostalCode {
    @Id
    private Integer productId;

    private Integer postalCodeId;

    public Integer getPostalCodeId() {
        return postalCodeId;
    }

    public void setPostalCodeId(Integer postalCodeId) {
        this.postalCodeId = postalCodeId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    
    public ProductPostalCode(){};

    public ProductPostalCode(Integer productId, Integer postalCodeId) {
        this.productId = productId;
        this.postalCodeId = postalCodeId;
    }
}
