package com.microservice.payment.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class UserBalance {
    @Id
    private int userId;
    private int price;
    
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public UserBalance(int userId, int price) {
		this.userId = userId;
		this.price = price;
	}
	public UserBalance() {
		super();
	}

}
