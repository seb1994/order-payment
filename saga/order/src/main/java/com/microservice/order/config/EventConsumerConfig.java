package com.microservice.order.config;

import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.util.commons.event.PaymentEvent;
import com.util.commons.event.ShippingEvent;

@Configuration
public class EventConsumerConfig {

    @Autowired
    private OrderStatusUpdateHandler handler;


    @Bean    
    public Consumer<PaymentEvent> paymentEventConsumer(){
    	System.out.println("-------paymentEventConsumer-------");
        return (payment)-> handler.updateOrder(payment.getPaymentRequestDto().getOrderId(),po->{
            po.setPaymentStatus(payment.getPaymentStatus());

        });
    }

    @Bean    
    public Consumer<ShippingEvent> shippingEventConsumer(){
    	System.out.println("-------shippingEventConsumer-------");
        return (shipping)-> handler.updateShippingOrder(shipping.getShippingRequestDto().getOrderId(),so->{
            so.setShippingStatus(shipping.getShippingStatus());
        });
    }
}
