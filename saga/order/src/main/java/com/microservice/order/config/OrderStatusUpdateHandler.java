package com.microservice.order.config;

import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import com.microservice.order.entity.PurchaseOrder;
import com.microservice.order.repository.OrderRepository;
import com.microservice.order.service.OrderStatusPublisher;
import com.util.commons.dto.OrderRequestDto;
import com.util.commons.status.OrderStatus;
import com.util.commons.status.PaymentStatus;
import com.util.commons.status.ShippingStatus;

@Configuration
public class OrderStatusUpdateHandler {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderStatusPublisher publisher;

    @Transactional
    public void updateOrder(int id, Consumer<PurchaseOrder> consumer) {
        repository.findById(id).ifPresent(consumer.andThen(this::updateOrder));
    }

    private void updateOrder(PurchaseOrder purchaseOrder) {
    	boolean isPaymentComplete = purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_COMPLETED);
        
        OrderStatus orderStatus;
        
        if(purchaseOrder.getShippingStatus() == null) {
        	orderStatus = OrderStatus.ORDER_SHIPPING_PENDNIG;
        }else if(isPaymentComplete && (purchaseOrder.getShippingStatus() == null || purchaseOrder.getShippingStatus().equals(ShippingStatus.SHIPPING_FAILED))) {
        	orderStatus = OrderStatus.ORDER_SHIPPING_PROBLEMS;
        }else if (!isPaymentComplete && (purchaseOrder.getShippingStatus() == null || purchaseOrder.getShippingStatus().equals(ShippingStatus.SHIPPING_FAILED))) {
        	orderStatus = OrderStatus.ORDER_CANCELLED;
        }else  if (isPaymentComplete && (purchaseOrder.getShippingStatus() != null && purchaseOrder.getShippingStatus().equals(ShippingStatus.SHIPPING_COMPLETED)))  {
        	orderStatus = OrderStatus.ORDER_COMPLETED;
        }else {
        	orderStatus = OrderStatus.ORDER_PAYMENTS_PROBLEMS;
        }
        
        purchaseOrder.setOrderStatus(orderStatus);
        
        if (!isPaymentComplete && orderStatus.equals(OrderStatus.ORDER_SHIPPING_PENDNIG)) {
            publisher.publishOrderEvent(convertEntityToDto(purchaseOrder), orderStatus);
        }
         
    }

    
    @Transactional
    public void updateShippingOrder(int id, Consumer<PurchaseOrder> consumer) {
        repository.findById(id).ifPresent(consumer.andThen(this::updateShippingOrder));
    }

    private void updateShippingOrder(PurchaseOrder purchaseOrder) {
        boolean isShippingComplete = ShippingStatus.SHIPPING_COMPLETED.equals(purchaseOrder.getShippingStatus());
        
        OrderStatus orderStatus;
        
        if(purchaseOrder.getPaymentStatus() == null) {
        	orderStatus = OrderStatus.ORDER_PAYMENT_PENDING;
        }else if(isShippingComplete && (purchaseOrder.getPaymentStatus() == null || purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_FAILED))) {
        	orderStatus = OrderStatus.ORDER_PAYMENTS_PROBLEMS;
        }else if (!isShippingComplete && (purchaseOrder.getPaymentStatus() == null || purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_FAILED))) {
        	orderStatus = OrderStatus.ORDER_CANCELLED;
        }else  if (isShippingComplete && (purchaseOrder.getPaymentStatus() != null && purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_COMPLETED)))  {
        	orderStatus = OrderStatus.ORDER_COMPLETED;
        }else {
        	orderStatus = OrderStatus.ORDER_SHIPPING_PROBLEMS;
        }
        
        purchaseOrder.setOrderStatus(orderStatus);
        
        if (!isShippingComplete) {
            publisher.publishOrderEvent(convertEntityToDto(purchaseOrder), orderStatus);
        }
        
    }
     
    
    
    public OrderRequestDto convertEntityToDto(PurchaseOrder purchaseOrder) {
        OrderRequestDto orderRequestDto = new OrderRequestDto();
        orderRequestDto.setOrderId(purchaseOrder.getId());
        orderRequestDto.setUserId(purchaseOrder.getUserId());
        orderRequestDto.setAmount(purchaseOrder.getPrice());
        orderRequestDto.setProductId(purchaseOrder.getProductId());
        return orderRequestDto;
    }
}
