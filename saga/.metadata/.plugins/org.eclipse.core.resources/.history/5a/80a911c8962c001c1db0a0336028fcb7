package com.microservice.payment.service;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.microservice.payment.entity.UserBalance;
import com.microservice.payment.entity.UserTransaction;
import com.microservice.payment.repository.UserBalanceRepository;
import com.microservice.payment.repository.UserTransactionRepository;
import com.util.commons.dto.OrderRequestDto;
import com.util.commons.dto.PaymentRequestDto;
import com.util.commons.event.OrderEvent;
import com.util.commons.event.PaymentEvent;
import com.util.commons.status.PaymentStatus;

@Service
public class PaymentService {

	@Autowired
	private UserBalanceRepository userBalanceRepository;
	@Autowired
	private UserTransactionRepository userTransactionRepository;

	@PostConstruct
	public void initUserBalanceInDB() {
		userBalanceRepository
				.saveAll(Stream.of(new UserBalance(101, 5000), 
						           new UserBalance(102, 3000), 
						           new UserBalance(103, 4200),
						           new UserBalance(104, 20000), 
						           new UserBalance(105, 999)).collect(Collectors.toList()));
	}

	@Transactional
	public PaymentEvent newOrderEvent(OrderEvent orderEvent) {
		OrderRequestDto orderRequestDto = orderEvent.getOrderRequestDto();

		PaymentRequestDto paymentRequestDto = new PaymentRequestDto(orderRequestDto.getOrderId(),orderRequestDto.getUserId(), orderRequestDto.getAmount());

		try {
			Thread.sleep(2000);
			return userBalanceRepository.findById(orderRequestDto.getUserId())
					.filter(ub -> ub.getPrice() > orderRequestDto.getAmount()).map(ub -> {
						ub.setPrice(ub.getPrice() - orderRequestDto.getAmount());
						userTransactionRepository.save(new UserTransaction(orderRequestDto.getOrderId(),orderRequestDto.getUserId(), orderRequestDto.getAmount()));
						return new PaymentEvent(paymentRequestDto, PaymentStatus.PAYMENT_COMPLETED);
					}).orElse(new PaymentEvent(paymentRequestDto, PaymentStatus.PAYMENT_FAILED));
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		
		return null;
		

	}

	@Transactional
	public void cancelOrderEvent(OrderEvent orderEvent) {

		userTransactionRepository.findById(orderEvent.getOrderRequestDto().getOrderId()).ifPresent(ut -> {
			userTransactionRepository.delete(ut);
			userTransactionRepository.findById(ut.getUserId())
					.ifPresent(ub -> ub.setAmount(ub.getAmount() + ut.getAmount()));
		});
	}
}
