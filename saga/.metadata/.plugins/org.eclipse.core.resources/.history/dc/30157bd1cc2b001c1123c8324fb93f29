package com.microservice.order.config;

import com.microservice.order.entity.PurchaseOrder;
import com.microservice.order.repository.OrderRepository;
import com.microservice.order.service.OrderStatusPublisher;
import com.util.commons.dto.OrderRequestDto;
import com.util.commons.status.OrderStatus;
import com.util.commons.status.PaymentStatus;
import com.util.commons.status.ShippingStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Consumer;

@Configuration
public class OrderStatusUpdateHandler {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderStatusPublisher publisher;

    @Transactional
    public void updateOrder(int id, Consumer<PurchaseOrder> consumer) {
        repository.findById(id).ifPresent(consumer.andThen(this::updateOrder));
    }

    private void updateOrder(PurchaseOrder purchaseOrder) {
        boolean isPaymentComplete = purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_COMPLETED);
        ShippingStatus shippingStatus = isPaymentComplete && ShippingStatus.SHIPPING_COMPLETED.equals(purchaseOrder.getShippingStatus()) ? ShippingStatus.SHIPPING_COMPLETED : ShippingStatus.SHIPPING_FAILED;
        
        
        
        //OrderStatus orderStatus = isPaymentComplete ? OrderStatus.ORDER_COMPLETED : OrderStatus.ORDER_CANCELLED;
        
        

        //purchaseOrder.setOrderStatus(orderStatus);
        //purchaseOrder.setShippingStatus(shippingStatus);
        
        if (!isPaymentComplete) {
            publisher.publishOrderEvent(convertEntityToDto(purchaseOrder), orderStatus);
        }
    }

    @Transactional
    public void updateShippingOrder(int id, Consumer<PurchaseOrder> consumer) {
        repository.findById(id).ifPresent(consumer.andThen(this::updateShippingOrder));
    }

    private void updateShippingOrder(PurchaseOrder purchaseOrder) {
        boolean isShippingComplete = ShippingStatus.SHIPPING_COMPLETED.equals(purchaseOrder.getShippingStatus());
        //boolean isPaymentComplete = PaymentStatus.PAYMENT_COMPLETED.equals(purchaseOrder.getPaymentStatus());
        OrderStatus orderStatus = isShippingComplete ?//&& isPaymentComplete ?
                                        OrderStatus.ORDER_COMPLETED :
                                            OrderStatus.ORDER_SHIPPING_PROBLEMS;
        purchaseOrder.setOrderStatus(orderStatus);
        if (!isShippingComplete) {
            publisher.publishOrderEvent(convertEntityToDto(purchaseOrder), orderStatus);
        }
    }

    public OrderRequestDto convertEntityToDto(PurchaseOrder purchaseOrder) {
        OrderRequestDto orderRequestDto = new OrderRequestDto();
        orderRequestDto.setOrderId(purchaseOrder.getId());
        orderRequestDto.setUserId(purchaseOrder.getUserId());
        orderRequestDto.setAmount(purchaseOrder.getPrice());
        orderRequestDto.setProductId(purchaseOrder.getProductId());
        return orderRequestDto;
    }
}
