package com.util.commons.event;


import java.util.Date;
import java.util.UUID;

import com.util.commons.dto.ShippingRequestDto;
import com.util.commons.status.ShippingStatus;

import lombok.Data;

@Data
public class ShippingEvent implements Event {
    private UUID eventId=UUID.randomUUID();
    private Date eventDate=new Date();
    private ShippingRequestDto shippingRequestDto;
    private ShippingStatus shippingStatus;

    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return eventDate;
    }

    public ShippingRequestDto getShippingRequestDto() {
		return shippingRequestDto;
	}

	public void setShippingRequestDto(ShippingRequestDto shippingRequestDto) {
		this.shippingRequestDto = shippingRequestDto;
	}

	public ShippingStatus getShippingStatus() {
		return shippingStatus;
	}

	public void setShippingStatus(ShippingStatus shippingStatus) {
		this.shippingStatus = shippingStatus;
	}

	public ShippingEvent(ShippingRequestDto shippingRequestDto, ShippingStatus shippingStatus) {
        this.shippingRequestDto = shippingRequestDto;
        this.shippingStatus = shippingStatus;
    }
}
