package com.util.commons.dto;

import lombok.Data;

@Data
public class OrderRequestDto {

    private Integer userId;
    private Integer productId;
    private Integer amount;
    private Integer orderId;
    private Integer postalCode;
    
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}
	public OrderRequestDto(Integer userId, Integer productId, Integer amount, Integer orderId, Integer postalCode) {
		this.userId = userId;
		this.productId = productId;
		this.amount = amount;
		this.orderId = orderId;
		this.postalCode = postalCode;
	}
	
	public OrderRequestDto() {
	
	}
	
	
	
}
