package com.util.commons.dto;

import lombok.Data;

@Data
public class ShippingRequestDto {


    private Integer orderId;
    private Integer productId;
    private Integer postalCodeId;
    
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getPostalCodeId() {
		return postalCodeId;
	}
	public void setPostalCodeId(Integer postalCodeId) {
		this.postalCodeId = postalCodeId;
	}
	
	public ShippingRequestDto(Integer orderId, Integer productId, Integer postalCodeId) {
		this.orderId = orderId;
		this.productId = productId;
		this.postalCodeId = postalCodeId;
	}
	
	
}
