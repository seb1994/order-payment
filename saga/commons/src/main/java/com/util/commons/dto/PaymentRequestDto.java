package com.util.commons.dto;

import lombok.Data;

@Data
public class PaymentRequestDto {

    private Integer orderId;
    private Integer userId;
    private Integer amount;
    
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	public PaymentRequestDto(Integer orderId, Integer userId, Integer amount) {
		this.orderId = orderId;
		this.userId = userId;
		this.amount = amount;
	}

	
}
